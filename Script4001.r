#loading countries.csv
countries <- read.csv("countries.csv", sep=";", row.names=1)
View(countries)

#reading data from australia
countries["Australia", ]

#row slice is a data frame
r <- countries["Australia", ]
class(r)
mean(r)

#converting data row to vector
class(countries["Chile", 1:4])
r <- as.numeric(countries["Chile", 1:4])
mean(r)

#var
var(as.numeric(countries["Chile", 1:4]))

#sd 
sqrt(var(as.numeric(countries["Chile", 1:4])))
sd(as.numeric(countries["Chile", 1:4]))

#mean from data row
#this will throw exception
countries$reservemean1 <- as.numeric(mean(as.numeric(countries[, 1:4])))
View(countries)

#mean from data row
#this works fine
mean(as.numeric(countries["Chile", 1:4]))
countries$reservemean <- as.numeric(rowMeans(countries[,5:8]))
View(countries)

# Bars Graph
pop <- table(countries$p2013 > 10000000)
barplot(pop, main="Population Over 10 Million ")

#hist
c <- rep(5,nrow(subset(countries,countries$p2010<=5000000 & countries$p2010>=0)))
c <- c(c,rep(10,nrow(subset(countries,countries$p2010<=100000000 & countries$p2010>5000000))))
c <- c(c,rep(15,nrow(subset(countries,countries$p2010<=150000000 & countries$p2010>10000000))))
c <- c(c,rep(20,nrow(subset(countries,countries$p2010>15000000))))

hist(c,col="green",main="Histogram Example")


#Gaussian/Bell
x <- sort(rnorm(11, m=0, sd=0.3))
y <- dnorm(x)
plot(x, y, type='l', xlab="Gaussian Distribution", ylab="Y Axis")








