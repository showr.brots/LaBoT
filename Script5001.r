#using if
test <- 10
if (test > 5) {
  print("Greater than five")
} else {
  print("Lower or equal to five")
}

#using ifelse
test <- 5
ifelse((test > 5),print("Greater than five"),print("Lower or equal to five"))

#using for
f <- c(1,2,3,4,5)
for (i in f) {
  print(paste("Line ",as.character(i)))
}

#creating a function
x <- c(1,2,3,3,4,5,6,76)

c <- 0

for (n in x) {
  if (n == 3) 
    c <- c+1 # greater than v
}

print(c)


valuecount <-function(x,v) {
  c <- 0 # int
  for (n in x) {
    if (n == v) 
      c <- c+1 # greater than v
  }
  return(c)
}

valuecount(c(1,2,3,3,4,5),3)

#sourcing a function
source("valuecount.r")
valuecount(c(1,2,3,4,5),4)

#ddply
library(plyr)
ddply(frame, "c1", function(x) {
  sum <- sum(x$c2)
  data.frame(sum = sum)
})
