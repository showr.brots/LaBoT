# Introdução ao R

R é uma linguagem e ambiente de código aberto para computação estatística e visualização. Atualmente na versão 3.3.2 ("Sincere Pumpkin Patch)"), pode ser considerado um dialeto da linguagem S, desenvolvida nos Laboratórios Bell por John Chambers, Rick Becker e Allan Wilks. Segundo Chambers, a linguagem S "alterou para sempre como as pessoas analisam, visualizam e manipulam dados". 

Com os avanços obtidos pela forte pesquisa, o R fornece uma ampla variedade de técnicas estatísticas, e está disponível para os principais sistemas operacionais dentro das liberdades do paradigma GNU, como possibilidade de alteração do código-fonte e gratuidade.

![John M. Chambers](.//images//JohnChambers.jpg)

O ambiente R permite manipular grandes volumes de dados, tendo a memória disponível como limite. Por se tratar de um ambiente de código aberto, permite ao usuário validar e inspecionar a arquitetura do sistema, contribuindo para a melhoria contínua das funções. É possível ainda vetorizar e paralelizar as operações, bem como escrever parte dos códigos em linguagem C, resultando em um considerável aumento de desempenho.

## História

Criada por Ross Ihaka e por Robert Gentleman no Departamento de Estatística da Universidade de Auckland, Nova Zelândia, conta com o esforço de uma forte comunidade mundial.

<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m11!1m3!1d378.8391858910909!2d174.76526411255742!3d-36.85651730632099!2m2!1f0!2f41.63765082430052!3m2!1i1024!2i768!4f20!3m3!1m2!1s0x0%3A0x34e95d8799957011!2sAuckland+University+of+Technology+-+City+Campus!5e1!3m2!1spt-BR!2sus!4v1475111227380" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Por que o R?

![](.//images//why.gif)

## Top universities for biological sciences

http://www.topuniversities.com/university-rankings/university-subject-rankings/2016/biological-sciences

<iframe src="http://www.topuniversities.com/university-rankings/university-subject-rankings/2016/biological-sciences" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Harvard University

http://isites.harvard.edu/icb/icb.do?keyword=k91599

<iframe src="http://isites.harvard.edu/icb/icb.do?keyword=k91599" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## University of Cambridge

http://bioinfotraining.bio.cam.ac.uk/postgraduate/programming/bioinfo-radv

<iframe src="http://bioinfotraining.bio.cam.ac.uk/postgraduate/programming/bioinfo-radv" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Oxford University

https://www.stats.ox.ac.uk/research

<iframe src="https://www.stats.ox.ac.uk/research" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## MIT 

http://libguides.mit.edu/c.php?g=176427&p=1159958

<iframe src="http://libguides.mit.edu/c.php?g=176427&p=1159958" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Stanford University

http://library.stanford.edu/projects/r

<iframe src="http://library.stanford.edu/projects/r" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## USP - Universidade de São Paulo

http://ecologia.ib.usp.br/bie5782/doku.php?id=start

<iframe src="http://ecologia.ib.usp.br/bie5782/doku.php?id=start" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Professor da USP

https://www.youtube.com/embed/o-tn0q2T--Y

<iframe width="712" height="400" src="https://www.youtube.com/embed/o-tn0q2T--Y" frameborder="1" allowfullscreen></iframe>

## Principal blog sobre R

[https://www.r-bloggers.com/](https://www.r-bloggers.com/) 

<iframe src="https://www.r-bloggers.com/" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

## Goooooooogle

[https://www.google.com.br](https://www.google.com.br) 

<iframe src="http://mrdoob.com/projects/chromeexperiments/google-gravity/" width="1024" height="400" frameborder="0" style="border:0" allowfullscreen alt="Carregando..."></iframe>

## Pelo próprio R

```{r, warning=FALSE, error=FALSE}
help.start()

```
## Livros


