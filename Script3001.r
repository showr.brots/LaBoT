#empty matrix
m <- matrix(nrow=2,ncol=2)
m[1,2] <- 1
m

#setting data to a matrix
m[1,1] <- 0
m[1,2] <- 1
m[2,1] <- 2
m[2,2] <- 3

#retrieve column value
m[ , 1]

#retrieve row value
m[1 , ]

#list
x <- list(c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))

class(x)
mode(x)

x <- c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55)
x[1]

x <- list(c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))
x[1]


#creating a data frame
c1 <- seq(1,10,2)
c2 <- seq(11,20,2)
c3 <- seq(21,30,2)

frame <- data.frame(c1,c2,c3)

print(frame)

#acessing first cell value
frame[1,1]
frame[1,"c1"]

#head and tails
head(frame,2)
tail(frame,2)

#first column vector
frame[[1]]
frame$c1
frame[,1]

#first row vector
frame[1,]
sum(frame[1,])
frame[1:3,]

#subset
subset(frame, frame$c1 > 3)

#working dir
print(getwd())
setwd("c:/tmp")

#load csv
read.csv("myfile.csv")
mydata <- read.csv("myfile.csv")
class(mydata)
mode(mydata)

#proptable
mydata$c4percent <- prop.table(mydata$col4)
print(mydata)

#simple plot example
plot(mydata$col1,mydata$col2)
lines(mydata$col2)

plot(mydata$col1,mydata$col2,main="Plot Example",xlab="Row Description ",ylab="Values ")
lines(mydata$col2)

#saving graphs to a pdf file
pdf("PlotExample.pdf",width=7,height=5)
plot(mydata$col1,mydata$col2,main="Plot Example",xlab="Row Description ",ylab="Values ")
lines(mydata$col2)
dev.off()

#saving graphs to a jpeg file
jpeg("PlotExample.pdf")
plot(mydata$col1,mydata$col2,main="Plot Example",xlab="Row Description ",ylab="Values ")
lines(mydata$col2)
dev.off()

#reading csv as data frame
read.csv("myfile.csv")
mydata <- read.csv("myfile.csv")

#creating new df
myframe <- data.frame(mydata$col1,mydata$col4)

#exporting data frame
write.table(myframe, file = "myfile.tx")

#importing 
newframe <- read.table("myfile.txt")
newframe