#basic set operation in R Language
n <- 10
print(n)
#addition
10 + 2
#subtraction
10 - 2
#multiplication
10*2
#division
10/2
#power
10^2
#mod
10%%2
#result of operation to a variable
r<-(10^2)*3
print(r)