#ls
ls()
#remove x
rm(x)
#declaring a string
str <- "R for Beginners"
#substring
substr("R for Beginners",7,15)
newvar <- substr("R for Beginners",7,15)
#sub function
sub("Beginners","Students",str)
#paste
paste(str," for students")
#upper and lower
toupper(str)
tolower(str)

modevar <- "R Programming"
mode(modevar)
length(modevar)

modevar <- 1000
mode(modevar)
length(modevar)

modevar <- TRUE
mode(modevar)
length(modevar)

mode(length)

class(str)

#empty matrix
m <- matrix(nrow=2,ncol=2)
m[1,2] <- 1
m

#setting data to a matrix
m[1,1] <- 0
m[1,2] <- 1
m[2,1] <- 2
m[2,2] <- 3

#retrieve column value
m[ , 1]

#retrieve row value
m[1 , ]

#concatenate
c(0,1,1,2,3,5,8,13,21,34,55)

#set to a variable 
x <- c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55)
print(x)
assign("x",c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))
print(x)
#fifth position
y <- x[5]
print(y)
#subset
z <- x[1:5]
print(z)
#average
print(mean(x))
#sum
sum(c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))
sum(x)
#length
length(c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))
length(x)
#average with sum and length
print(sum(x)/length(x))
#seq 1 - 10
seq(1,10)
#seq 1.2 - 10
seq(1.2,10)
#seq 1 - 10 2 by 2
seq(1,10,2)
#min
min(x)
#max
max(x)
#prod
prod(x)
prod(x[2:11])
#range
range(x)
#sample
sample(1:10)
sample(5:15,6)
sample(5:15,5,replace=T)
#median
median(x)
#mode
modevar <- "R Programming"
mode(modevar)
length(modevar)
modevar <- 1000
mode(modevar)
length(modevar)
modevar <- TRUE
mode(modevar)
length(modevar)
mode(length)
#mode x
mode(x)
#class
class(x)
#list function
x <- list(c(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55))
class(x)
mode(x)
x<-as.vector(x,mode="numeric")
class(x)